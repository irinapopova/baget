<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Сайт для расчета рам от компании Всеподрамники</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://d1azc1qln24ryf.cloudfront.net/114779/Socicon/style-cf.css?rd5re8">
  </head>
  <body>
    <header class="main-header inner-page section-overlay" style="background-image: url(img/header-image.jpg);">
      <div class="container ">
        <div class="container-one">
          <nav class="nav">
            <ul class="main-menu">
              <li><a class="nav-link activ">Главная</a></li>
              <li><a class="nav-link" href="#platform">Как пользоваться сайтом</a></li>
              <li><a class="nav-link" href="#variants">Варианты рам для расчета</a></li>
              <li><a class="nav-link" href="#footer-wrap">Связаться с нами</a></li>
            </ul>
          </nav>
          <div class="content">
            <img src="img/logo.png" alt=""><img style="margin-left:40px" src="/logonew.png">
            <h1>Сайт для расчета стоимости и отправки<br> заказов на рамы, подрамники и багеты</h1>
            <div class="descr">
              Здесь вы можете воспользоваться калькулятором для расчета стоимости рам, подрамников и багетов, после этого можно отправить вашу заявку. <br>По всем возникающим вопросам обращайтесь к нашим менеджерам по телефону <a href="tel:+79818818212">+7 (981) 881-82-12</a>.
            </div>
            <a class="button" href="#variants">Рассчитать сейчас</a>
          </div>
        </div>
      </div>
    </header>
    <section id="platform">
      <div class="container">
        <h2 class="about-item">Как <b>пользоваться</b></h2>
        <h3 class="name-item">данным сайтом (информация для новых пользователей)</h3>
        <div class="items">
          <div class="row">
            <div class="col"><img src="img/im1.png" alt=""></div>
            <div class="col">
              <h4 class="name-col">1. Выбрать тот товар, который вам необходимо рассчитать.</h4>
              <div class="text">
                <p>На данный момент у нас есть пять калькуляторов для расчета индивидуальных заказов:</p>
                <ul>
                  <li>Багет</li>
                  <li>Подрамник</li>
                  <li>Рама, плоский профиль, сосна</li>
                  <li>Рама, полукруглый профиль, сосна</li>
                  <li>Рама, широкий полукруглый профиль, сосна</li>
                  <li>Натяжка холста на подрамник</li>
                </ul>
                <p>После нажатия на изображение или название необходимого товара вы попадете в соответствующий калькулятор.</p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col"><img src="img/im2.png" alt=""></div>
            <div class="col">
              <h4>2. Заполнить все необходимые поля для расчета в калькуляторе.</h4>
              <div class="text">
                <p>Вам необходимо заполнить желаемые размеры для расчета стоимости товара и контактные данные.</p>
                <p>Важно: внимательно проверяйте корректность заполнения контактных данных, иначе нашим менеджерам будет
                сложно связаться с вами.</p>
                <p>После отправки данных вы увидите сообщение об успешной отправке. Если этого не произошло, попробуйте еще раз или позвоните нашим менеджерам по телефону <a href="tel:+79818818212">+7 (981) 881-82-12</a>.</p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col"><img src="img/im3.png" alt=""></div>
            <div class="col">
              <h4>3. Дождаться звонка нашего менеджера и получить ваш заказ.</h4>
              <div class="text">
                <p>После оформления и отправки заявки наши менеджеры свяжутся с вами в кратчайшие сроки.</p>
                <p>Важно: если по каким-то причинам вам не перезвонили или не написали на почту, звоните по телефону <a href="tel:+79818818212">+7 (981) 881-82-12</a> или пишите на почту <a href="mailto:info@vsepodramniki.ru">info@vsepodramniki.ru</a>.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="product">
      <div class="container">
        <div class="product-description">
          <div class="row-1">
            <h2 class="name-note">Что-то не понятно?</h2>
            <div class="note">Позвоните нам прямо сейчас и мы вам всё объясним.</div>
            <div class="phone"><a href="tel:+79818818212">+7 (981) 881-82-12</a></div>
          </div>
      </div>
    </section>
    <section id="variants">
      <div class="container">
        <div class="for-title">
          <h2>Выберите необходимое изделие или услугу для расчета</h2>
          <p>Выберите то изделие или услугу, которое хотите рассчитать и заказать. Нажмите на него и вы попадёте в калькулятор для расчета.</p>
        </div>
        <div class="team-list">
          <div class="worker">
            <div class="for-photo">
              <div class="bd"></div>
              <a href="/baget.html"><div class="foto"  style="background-image: url(/img/baget.jpg);"></div></a>
            </div>
            <a href="/baget.html"><div class="nick-name">Багет</div></a>
            
          </div>
          <div class="worker">
            <div class="for-photo">
              <div class="bd"></div>
              <a href="/glyhoi.html"><div class="foto"  style="background-image: url(/img/podramnik.jpg);"></div></a>
            </div>
            <div class="nick-name"><a href="/glyhoi.html">Глухой (готовый)<br> подрамник</a></div>
            
          </div>
          <div class="worker">
            <div class="for-photo">
              <div class="bd"></div>
              <a href="/natyajka.html"><div class="foto"  style="background-image: url(/img/natyajka.jpg);"></div></a>
            </div>
            <a href="/natyajka.html"><div class="nick-name">Натяжка</div></a>
            
          </div>
           <div class="worker">
            <div class="for-photo">
              <div class="bd"></div>
              <a href="/modul.html"><div class="foto"  style="background-image: url(/img/Modul.jpg);"></div></a>
            </div>
            <div class="nick-name"><a href="/modul.html">Модульный подрамник</a></div>
            
          </div>
          <div class="worker">
            <div class="for-photo">
              <div class="bd"></div>
              <a href="/rama.html"><div class="foto" style="background-image: url(/img/rama-polykr-profil.jpg);"></div></a>
            </div>
            <div class="nick-name"><a href="/rama.html">Рама, </a></div>
            <div class="spec">полукруглый профиль, сосна</div>
            
          </div>
           <div class="worker">
            <div class="for-photo">
              <div class="bd"></div>
              <a href="/rama2.html"><div class="foto"  style="background-image: url(/img/rama-pl-profil-sosna.jpg);"></div></a>
            </div>
            <a href="/rama2.html"><div class="nick-name">Рама, </div></a>
            <div class="spec">плоский профиль, сосна</div>
            
          </div>
          <div class="worker">
            <div class="for-photo">
              <div class="bd"></div>
              <a href="/rama3.html"><div class="foto" style="background-image: url(/img/rama-shir-profil.jpg);"></div></a>
            </div>
            <a href="/rama3.html"><div class="nick-name">Рама, </div></a>
            <div class="spec">широкий полукруглый профиль, сосна</div>    
          </div>
        </div>
      </div>
    </section>
    <footer id="footer-wrap">
      <div class="container">
        <div class="col-social col-footer">
          <h3 class="footer-caption">Мы в соцсетях</h3>
          <div class="footer-social">
            <a target="_blank" href="https://www.facebook.com/groups/vsepodramniki/" title="фейсбук" class="social-btn social-btn-f"><span class="socicon-facebook"></span></a>
            <a target="_blank" href="https://vk.com/vsepodramniki" title="вконтакте" class="social-btn social-btn-"><span class="socicon-vkontakte"></span></a>
            <a target="_blank" href="https://ok.ru/group/53739461083218" title="одноклассники" class="social-btn social-btn-"><span class="socicon-odnoklassniki"></span></a>
          </div>
        </div>
        <div class="col-mail col-footer">
          <h3 class="footer-caption">Наш основной сайт</h3>
          <a target="_blank" href="https://vsepodramniki.ru">https://vsepodramniki.ru</a>
        </div>
        <div class="col-contact col-footer">
          <h3 class="footer-caption">Контактная информация</h3>
          <div class="info-contact">
            <div class="adress">
              Спб, ул. Кирочная д.22
             <br>Пн-Вс., 10:00-20:00
            </div>
            <div class="info-mail">
              +7 (981) 881-82-12 <br><a href="mailto:info@vsepodramniki.ru">info@vsepodramniki.ru</a>
            </div>
          </div>
          <div class="copyright">&copy;
            2018. Все права защищены.
          </div>
        </div>
      </div>
    </footer>
  </body>
</html>
