<section id="product">
      <div class="container">
        <div class="product-description">
          <div class="row-1">
            <h2 class="name-note">Что-то не понятно?</h2>
            <div class="note">Позвоните на прямо сейчас и мы вам всё объясним.</div>
            <div class="phone"><a href="tel:+79818818212">+7 (981) 881-82-12</a></div>
          </div>
      </div>
    </section>
    <footer id="footer-wrap">
      <div class="container">
        <div class="col-social col-footer">
          <h3 class="footer-caption">Мы в соцсетях</h3>
          <div class="footer-social">
            <a href="https://www.facebook.com/groups/vsepodramniki/" title="фейсбук" class="social-btn social-btn-f"><span class="socicon-facebook"></span></a>
            <a href="#" title="" class="social-btn social-btn-"><span class="socicon-twitter"></span></a>
            <a href="#" title="" class="social-btn social-btn-"><span class="socicon-linkedin"></span></a>
          </div>
        </div>
        <div class="col-mail col-footer">
          <h3 class="footer-caption">Наш основной сайт</h3>
          <a href="https://vsepodramniki.ru">https://vsepodramniki.ru</a>
        </div>
        <div class="col-contact col-footer">
          <h3 class="footer-caption">Контактная информация</h3>
          <div class="info-contact">
            <div class="adress">
              Спб, ул. Кирочная д.22
             <br>Пн-Вс., 10:00-20:00
            </div>
            <div class="info-mail">
              +7 (981) 881-82-12 <br><a href="mailto:info@vsepodramniki.ru">info@vsepodramniki.ru</a>
            </div>
          </div>
          <div class="copyright">&copy;
            2018. Все права защищены.
          </div>
        </div>
      </div>
    </footer>
    <script src="js/script.js"></script>
  </body>
</html>
