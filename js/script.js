document.addEventListener('DOMContentLoaded', function() {
	function showDiv(elem) {
      if (elem.value == 'Да') {
          document.getElementById('hidden_div').style.display = "table-row";
      }
  }

  function onFileSelected(event) {
		if (FileReader) {
			var selectedFile = event.target.files[0];
		  var reader = new FileReader();

		  var imgtag = document.getElementById("myimage");
		  imgtag.title = selectedFile.name;

		  reader.onload = function(event) {
		    imgtag.src = event.target.result;
		  };

		  reader.readAsDataURL(selectedFile);
		} else {
			console.log('FileReader не поддерживается в этом браузере!')
		}							  
	}

	function checkPaspartu() {
		var inputWidth = $('input[name="width"]'),
				inputHeight = $('input[name="height"]'),
				selectSteklo = $('select[name="steklo"]'),
				selectPaspartu = $('select[name="paspartu"]'),
				selectZadnik = $('select[name="zadnik"]');

 	 	if ((inputWidth.val() <= 80 && inputHeight.val() <=60) || 
 	 		(inputWidth.val() <= 60 && inputHeight.val() <= 80)) {
				$('body').on('change', 'select[name="paspartu"]', function(){
					if (selectPaspartu.val() == "Да") {
						$('.paspartuHeight').show();
						$('.paspartuWidth').show();
						selectSteklo.val("Да");
						selectZadnik.val("Да");
					} else {
						$('.paspartuHeight').hide();
						$('.paspartuWidth').hide();
						selectPaspartu.val("Нет");
						selectSteklo.val("Нет");
						selectZadnik.val("Нет");
					}
				});
			} else {
					selectPaspartu.val("Нет");
					selectSteklo.val("Нет");
					selectZadnik.val("Нет");
			}
		}

		// ищет только по первым 3м цифрам (доработать??)
		function filter() {
			var keyword = document.getElementById("search").value;
			var select = document.getElementById("select");
			for (var i = 0; i < select.length; i++) {
				var txt = select.options[i].text;
				if (txt.substring(0, keyword.length).toLowerCase() !== keyword.toLowerCase() && keyword.trim() !== "") {
				  $(select.options[i]).attr('disabled', 'disabled').hide();
				} else {
				  $(select.options[i]).removeAttr('disabled').show();
				}
			}
		}


		(function ($) {
			$(function(){
				$("ul.main-menu li").hover(function(){
					$(this).addClass("hover");
					$('ul:first',this).css('visibility', 'visible');
				}, function(){
					$(this).removeClass("hover");
					$('ul:first',this).css('visibility', 'hidden');
				});
				$("ul.main-menu li ul li:has(ul)").find("a:first").append(" &raquo; ");
			});

			$(function () {
					$('select[name="article"]').change(function(){
					 	if($(this).find(':selected').data('othervalue') == "454") {
							$('input[name="widthx"]').val('41');
							$('input[name="pricex"]').val('215.6');
						}
						if($(this).find(':selected').data('othervalue') == "453") {
							$('input[name="widthx"]').val('46');
							$('input[name="pricex"]').val('385');
						}
						if($(this).find(':selected').data('othervalue') == "456") {
							$('input[name="widthx"]').val('51');
							$('input[name="pricex"]').val('376.2');
						}
						if($(this).find(':selected').data('othervalue') == "271") {
							$('input[name="widthx"]').val('56');
							$('input[name="pricex"]').val('345.4');
						}
						if($(this).find(':selected').data('othervalue') == "337") {
							$('input[name="widthx"]').val('37');
							$('input[name="pricex"]').val('462');
						}
						if($(this).find(':selected').data('othervalue') == "281") {
							$('input[name="widthx"]').val('32');
							$('input[name="pricex"]').val('209');
						}
						if($(this).find(':selected').data('othervalue') == "446") {
							$('input[name="widthx"]').val('40');
							$('input[name="pricex"]').val('290.4');
						}
						if($(this).find(':selected').data('othervalue') == "520") {
							$('input[name="widthx"]').val('33');
							$('input[name="pricex"]').val('275');
						}
						if($(this).find(':selected').data('othervalue') == "272") {
							$('input[name="widthx"]').val('56');
							$('input[name="pricex"]').val('360.8');
						}
						if($(this).find(':selected').data('othervalue') == "284") {
							$('input[name="widthx"]').val('37');
							$('input[name="pricex"]').val('117.7');
						}
						if($(this).find(':selected').data('othervalue') == "283") {
							$('input[name="widthx"]').val('29');
							$('input[name="pricex"]').val('173.8');
						}
						if($(this).find(':selected').data('othervalue') == "029") {
							$('input[name="widthx"]').val('23');
							$('input[name="pricex"]').val('330');
						}
						if($(this).find(':selected').data('othervalue') == "340") {
							$('input[name="widthx"]').val('45');
							$('input[name="pricex"]').val('420.2');
						}
						if($(this).find(':selected').data('othervalue') == "409") {
							$('input[name="widthx"]').val('20');
							$('input[name="pricex"]').val('132');
						}
						if($(this).find(':selected').data('othervalue') == "523") {
							$('input[name="widthx"]').val('15');
							$('input[name="pricex"]').val('129.8');
						}
						$('input[name="widthx"]').change();
						$('input[name="pricex"]').change();
					 });

						var field = new Array("name","phone", "mail","article","vn_number","comments","in_frame","podves","podves_tip");

						$("#form-calc").submit(function() {
							var error = 0;
							$("#form-calc").find(":input").each(function() {
								for (var i = 0; i < field.length; i++) {
									if ($(this).attr("name") == field[i]) { 
										if (!$(this).val()) {
											$(this).css("background-color", "#fde4e4");
											error = 1;						
										}
									}						
								}			
							});
							if (error == 0) {
								var $form = $("#form-calc"),
								s_phone = $form.find( "input[name='phone']" ).val(),
								s_vn_number = $form.find( "input[name='vn_number']" ).val(),
								s_comments = $form.find( "textarea[name='comments']" ).val(),
								s_name = $form.find( "input[name='name']" ).val(),
								s_article = $form.find( "select[name='article']" ).val(),
								s_mail = $form.find( "input[name='mail']" ).val(),
								s_price = $form.find( "input[name='price']" ).val(),
								s_in_frame = $form.find( "select[name='in_frame']" ).val(),
								s_podves = $form.find( "select[name='podves']" ).val(),
								s_podves_tip = $form.find( "select[name='podves_tip']" ).val(),
								s_form = $form.find( "input[name='form']" ).val(),
								s_w = $form.find( "input[name='width']" ).val(),
								s_h = $form.find( "input[name='height']" ).val(),
								s_paspartu = $form.find( "select[name='paspartu']" ).val(),
								paspartuWidth = $form.find( "input[name='paspartuWidth']").val(),
								paspartuHeight = $form.find( "input[name='paspartuHeight']").val(),
								s_steklo = $form.find( "select[name='steklo']" ).val(),
								s_zadnik = $form.find( "select[name='zadnik']" ).val(),
								url = $form.attr( "action" );
								$( ".result" ).fadeIn(300);

								$.post( url, {
									phone: s_phone,
									vn_number: s_vn_number,
									podves: s_podves,
									podves_tip: s_podves_tip,
									in_frame: s_in_frame,
									comments: s_comments,
									name: s_name,
									form: s_form,
									article: s_article,
									mail: s_mail,
									price: s_price,
									w: s_w,
									h: s_h,
									paspartu: s_paspartu,
									paspartuWidth: paspartuWidth,
									paspartuHeight: paspartuHeight,
									s_steklo: s_steklo,
									s_zadnik: s_zadnik
								} ).done(function(data) {});

								return false;
							} else {
								return false;
							}
						});
					 
							calculate();
							jQuery('#calculator input').keyup(function() {
								this.value = this.value.replace(/[^0-9\.,]/g, '');
								this.value = this.value.replace(/[,]/, '.');
							});
							jQuery('#calculator input, #calculator select').change(function() {
								calculate();
							});
							jQuery('#calculator input').keyup(function() {
								calculate();
							});


							function calculate() {
						  	$('.calculator').each(function(key, val){
									calcInputs = {};
									$(this).find('input[type="number"], select').each(function(key, val){
										name = $(this).attr('name');
										val = $(this).val();
										if (!$.isNumeric(val)) {
											switch (name) {
												case 'width':
													val = '';
													break;
												case 'height':
													val = '';
													break;
												case 'pricex':
													val = '';
													break;
												default:
													break;
											}
											$(this).val(val);
										}
										calcInputs[name] = val;
									});
									total = 0;
									rs = 0;

                  var MetrKvadr = (parseFloat(calcInputs.width) * parseFloat(calcInputs.height)) / 1000;   // квадратные метры картины

                  if (calcInputs.widthx > 0 && calcInputs.widthx <= 10) {
										rs = 100;
									}
									if (parseFloat(calcInputs.widthx) >= 10.01 && parseFloat(calcInputs.widthx) <= 20) {
										rs = 200;
									}
									if (parseFloat(calcInputs.widthx) >= 20.01 && parseFloat(calcInputs.widthx) <= 40) {
										rs = 300;
									}
									if (parseFloat(calcInputs.widthx) >= 40.01 && parseFloat(calcInputs.widthx) <= 60) {
										rs = 400;
									}
									if (parseFloat(calcInputs.widthx) >= 60.01 && parseFloat(calcInputs.widthx) <= 100) {
										rs = 500;
									}
									if (parseFloat(calcInputs.widthx) >= 60.01 && parseFloat(calcInputs.widthx) <= 100) {
										rs = 500;
									}
									if (parseFloat(calcInputs.widthx) >= 40.01 && parseFloat(calcInputs.widthx) <= 60 && parseFloat(calcInputs.height) > 140) {
										rs = 600;
									}
									if (parseFloat(calcInputs.widthx) >= 60.01 && parseFloat(calcInputs.widthx) <= 100 && parseFloat(calcInputs.height) > 140) {
										rs = 700;
									}
									total += ((((parseFloat(calcInputs.width)+parseFloat(calcInputs.height))*2)+((parseFloat(calcInputs.widthx)/10)*8))/100);
							    // console.log(total);
							    total += total*(parseFloat(calcInputs.pricex));
							    // console.log(total);
							    total += rs;
							    // console.log(total);
								if(calcInputs.in_frame == 'Да'){
									if(parseFloat(calcInputs.width) < 140 && parseFloat(calcInputs.height) < 140){
										total += 100;
									} else {
										total += 200;
									}
								}
								if(calcInputs.podves == 'Да'){
									if(parseFloat(calcInputs.width) < 30){
										total += 125;
									} else if (parseFloat(calcInputs.width) >= 30 && parseFloat(calcInputs.width) < 50){
										total += 130;
									} else if (parseFloat(calcInputs.width) >= 50 && parseFloat(calcInputs.width) < 100){
										total += 145;
									} else {
										total += 150;
									}
								}
								if (calcInputs.paspartu == 'Да') {
								    if ((parseFloat(calcInputs.width) >= 40.7 && parseFloat(calcInputs.height) >= 50.8) || (parseFloat(calcInputs.width) >= 50.8 && parseFloat(calcInputs.height) >= 40.7)) {
                                        total += 900;
                                    } else {
								        total += 600;
									}
									total += 200; //вырез окна
								}
								if (calcInputs.steklo == 'Да') {
								    if ((parseFloat(calcInputs.width) <= 80 && parseFloat(calcInputs.height) <= 60) || (parseFloat(calcInputs.width) <= 60 && parseFloat(calcInputs.height) <= 80)) {
								        var steklo_price = 1300 * MetrKvadr;
								        total += steklo_price;
									}
								}
								if (calcInputs.zadnik == 'Да') {
									var zadnik_price = 350 * MetrKvadr;
									total += zadnik_price;
								}
								// console.log(rs);
								// console.log(calcInputs.in_frame);
								// console.log(calcInputs.pricex);
								total = Math.floor(Math.floor(total)*1.7); // 1.7 наценка 70%
								totalx = Math.floor(total);
								total += ' руб.';
								jQuery(this).find('span.total').html(total);
								jQuery('input[name="price"]').val(totalx);
						  });
							}
						});
		})(jQuery);
});
