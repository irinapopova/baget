<?php include 'parts/header.php'; ?>
    <header class="main-header section-overlay" style="background-image: url(img/header-image.jpg);">
      <div class="container ">
        <div class="container-one">
          <nav class="nav">
            <ul class="main-menu">
              <li><a class="nav-link" href="/">Главная</a></li>
              <li><a class="nav-link" href="#rachet">Рассчитать багет под заказ</a></li>
              </li>
              <li><a class="nav-link" href="#">Список калькуляторов</a>
              	<ul class="sub_menu">
              		<li class="submenu-item"><a href="/baget.html">Багет</a></li>
              		<li class="submenu-item"><a href="/glyhoi.html">Глухой подрамник</a></li>
              		<li class="submenu-item"><a href="/natyajka.html">Натяжка</a></li>
              		<li class="submenu-item"><a href="/modul.html">Модульный подрамник</a></li>
              		<li class="submenu-item"><a href="/rama.html">Рама (полукр.профиль)</a></li>
              		<li class="submenu-item"><a href="/rama2.html">Рама (плоский профиль)</a></li>
              		<li class="submenu-item"><a href="/rama3.html">Рама (широк.полукр.профиль)</a></li>
                </ul>
              </li>
              <li><a class="nav-link" href="#footer-wrap">Связаться с нами</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </header>
    <section id="rachet">
      <div class="container">
        <h2 class="about-item">Калькулятор расчета</h2>
        <h3 class="name-item">для багета под заказ</h3>
        <img style="display: block;
  margin: 0 auto;" src="/logonew.png">
        <div class="items">
          <div class="row">
			<div class="price">
				<b>Введите высоту и ширину рамы, выберите артикул багета, отметьте необходимые доп. услуги. </b>
				<form action="send.php" method="post" id="form-calc">
				<div id="calculator" class="calculator">
				<table>
					<colgroup>
						<col width="70%">
						<col width="30%">
					</colgroup>
					<tbody>
						<tr class="required">
							<td>Ширина изделия</td>
							<td><input id="numberex" name="width" type="number" step="0.01" value="40"></td>
							<td>см</td>
						</tr>
						<tr>
							<td>Высота изделия</td>
							<td><input id="numberex" name="height" type="number" step="0.01" value="40"></td>
							<td>см</td>
						</tr>
						<tr>
							<td style="padding-bottom: 10px;">Артикул багета:</td>
						</tr>
						<tr>
							<td style="vertical-align: text-bottom;">Поиск по артикулу</td>
							<td><input type="text" id="search" name="search" style="" placeholder="Введите первые 3 цифры" onkeyup="filter()">
						</tr>
						<tr>
							<td style="vertical-align: text-bottom;">Выбор из полного списка артикулов</td>
						<td>
								<select id="select" size="10" name="article">
										<option value="029-02" data-othervalue="029" data-width="23">029-02</option>
										<option value="029-16" data-othervalue="029" data-width="23">029-16</option>
										<option value="029-18" data-othervalue="029" data-width="23">029-18</option>
										<option value="029-22" data-othervalue="029" data-width="23">029-22</option>
										<option value="029-23" data-othervalue="029" data-width="23">029-23</option>
										<option value="029-24" data-othervalue="029" data-width="23">029-24</option>
										<option value="029-04" data-othervalue="029" data-width="23">029-04</option>
										<option value="029-07" data-othervalue="029" data-width="23">029-07</option>
										<option value="029-09" data-othervalue="029" data-width="23">029-09</option>
										<option value="029-10" data-othervalue="029" data-width="23">029-10</option>
										<option value="029-12" data-othervalue="029" data-width="23">029-12</option>
										<option value="029-21" data-othervalue="029" data-width="23">029-21</option>
										<option value="029-25" data-othervalue="029" data-width="23">029-25</option>
										<option value="029-08" data-othervalue="029" data-width="23">029-08</option>
										<option value="029-15" data-othervalue="029" data-width="23">029-15</option>
										<option value="029-20" data-othervalue="029" data-width="23">029-20</option>
										<option value="029-05" data-othervalue="029" data-width="23">029-05</option>
										<option value="409-02" data-othervalue="409" data-width="20">409-02</option>
										<option value="409-03" data-othervalue="409" data-width="20">409-03</option>
										<option value="409-05" data-othervalue="409" data-width="20">409-05</option>
										<option value="409-06" data-othervalue="409" data-width="20">409-06</option>
										<option value="523-06" data-othervalue="523" data-width="15">523-06</option>
										<option value="523-07" data-othervalue="523" data-width="15">523-07</option>
										<option value="523-08" data-othervalue="523" data-width="15">523-08</option>
										<option value="454-01" data-othervalue="454" data-width="41">454-01</option>
										<option value="454-02" data-othervalue="454" data-width="41">454-02</option>
										<option value="454-03" data-othervalue="454" data-width="41">454-03</option>
										<option value="454-04" data-othervalue="454" data-width="41">454-04</option>
										<option value="454-05" data-othervalue="454" data-width="41">454-05</option>
										<option value="454-07" data-othervalue="454" data-width="41">454-07</option>
										<option value="453-01" data-othervalue="453" data-width="46">453-01</option>
										<option value="453-02" data-othervalue="453" data-width="46">453-02</option>
										<option value="453-03" data-othervalue="453" data-width="46">453-03</option>
										<option value="453-04" data-othervalue="453" data-width="46">453-04</option>
										<option value="453-05" data-othervalue="453" data-width="46">453-05</option>
										<option value="453-06" data-othervalue="453" data-width="46">453-06</option>
										<option value="453-07" data-othervalue="453" data-width="46">453-07</option>
										<option value="456-01" data-othervalue="456" data-width="51">456-01</option>
										<option value="456-02" data-othervalue="456" data-width="51">456-02</option>
										<option value="456-03" data-othervalue="456" data-width="51">456-03</option>
										<option value="456-04" data-othervalue="456" data-width="51">456-04</option>
										<option value="456-05" data-othervalue="456" data-width="51">456-05</option>
										<option value="456-06" data-othervalue="456" data-width="51">456-06</option>
										<option value="271-01" data-othervalue="271" data-width="56">271-01</option>
										<option value="271-02" data-othervalue="271" data-width="56">271-02</option>
										<option value="271-03" data-othervalue="271" data-width="56">271-03</option>
										<option value="271-04" data-othervalue="271" data-width="56">271-04</option>
										<option value="271-05" data-othervalue="271" data-width="56">271-05</option>
										<option value="271-06" data-othervalue="271" data-width="56">271-06</option>
										<option value="337-01" data-othervalue="337" data-width="37">337-01</option>
										<option value="337-02" data-othervalue="337" data-width="37">337-02</option>
										<option value="337-03" data-othervalue="337" data-width="37">337-03</option>
										<option value="340-04" data-othervalue="340" data-width="45">340-04</option>
										<option value="340-05" data-othervalue="340" data-width="45">340-05</option>
										<option value="281-01" data-othervalue="281" data-width="32">281-01</option>
										<option value="281-02" data-othervalue="281" data-width="32">281-02</option>
										<option value="281-03" data-othervalue="281" data-width="32">281-03</option>
										<option value="281-04" data-othervalue="281" data-width="32">281-04</option>
										<option value="281-05" data-othervalue="281" data-width="32">281-05</option>
										<option value="281-06" data-othervalue="281" data-width="32">281-06</option>
										<option value="281-07" data-othervalue="281" data-width="32">281-07</option>
										<option value="281-08" data-othervalue="281" data-width="32">281-08</option>
										<option value="281-09" data-othervalue="281" data-width="32">281-09</option>
										<option value="281-10" data-othervalue="281" data-width="32">281-10</option>
										<option value="281-11" data-othervalue="281" data-width="32">281-11</option>
										<option value="446-01" data-othervalue="446" data-width="40">446-01</option>
										<option value="446-02" data-othervalue="446" data-width="40">446-02</option>
										<option value="446-03" data-othervalue="446" data-width="40">446-03</option>
										<option value="446-04" data-othervalue="446" data-width="40">446-04</option>
										<option value="446-05" data-othervalue="446" data-width="40">446-05</option>
										<option value="446-06" data-othervalue="446" data-width="40">446-06</option>
										<option value="520-01" data-othervalue="520" data-width="33">520-01</option>
										<option value="520-02" data-othervalue="520" data-width="33">520-02</option>
										<option value="520-03" data-othervalue="520" data-width="33">520-03</option>
										<option value="520-04" data-othervalue="520" data-width="33">520-04</option>
										<option value="520-05" data-othervalue="520" data-width="33">520-05</option>
										<option value="520-06" data-othervalue="520" data-width="33">520-06</option>
										<option value="272-01" data-othervalue="272" data-width="56">272-01</option>
										<option value="272-02" data-othervalue="272" data-width="56">272-02</option>
										<option value="272-03" data-othervalue="272" data-width="56">272-03</option>
										<option value="272-04" data-othervalue="272" data-width="56">272-04</option>
										<option value="284-01" data-othervalue="284" data-width="37">284-01</option>
										<option value="283-01" data-othervalue="283" data-width="29">283-01</option>
										<option value="283-02" data-othervalue="283" data-width="29">283-02</option>
										<option value="283-03" data-othervalue="283" data-width="29">283-03</option>
										<option value="283-04" data-othervalue="283" data-width="29">283-04</option>
										<option value="283-05" data-othervalue="283" data-width="29">283-05</option>
										<option value="283-06" data-othervalue="283" data-width="29">283-06</option>
										<option value="283-07" data-othervalue="283" data-width="29">283-07</option>
										<option value="283-08" data-othervalue="283" data-width="29">283-08</option>
										<option value="283-09" data-othervalue="283" data-width="29">283-09</option>
									</select>
								</td>
							
						</tr>
						<tr style="display: none;">
							<td>Ширина багета</td>
							<td><input name="widthx" type="number" value="41" disabled></td>
						</tr>
						<tr>
							<td>Изображение</td>
							<td>
								<input type="file" onchange="onFileSelected(event)">
								<img id="myimage" style="max-height: 200px; max-width: 100%;">
							</td>
						</tr>
						<tr>
							<td>Установка картины в раму</td>
							<td>
								<select name="in_frame">
									<option value="Нет">Нет</option>
									<option value="Да">Да</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Услуга по установке подвески <br>(+трос и фурнитура)</td>
							<td>
								<select name="podves" onchange="showDiv(this)">
									<option value="Нет">Нет</option>
									<option value="Да">Да</option>
								</select>
							</td>
						</tr>
						
						<tr id="hidden_div" style="display: none;">
							<td>Тип подвески</td>
							<td>
								<select name="podves_tip">
									<option value="По горизонтали">По горизонтали</option>
									<option value="По вертикали">По вертикали</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Нужно ли вам паспарту?</td>
							<td>
								<select name="paspartu" onchange="checkPaspartu()">
									<option value="Нет">Нет</option>
									<option value="Да">Да</option>
								</select>
							</td>
						</tr>

						<tr style="display: none;" class="paspartuWidth">
							<td>Размер по горизонтали</td>
							<td><input type="text" name="paspartuWidth"></td>
						</tr>
						<tr style="display: none;" class="paspartuHeight">
							<td>Размер по вертикали</td>
							<td><input type="text" name="paspartuHeight"></td>
						</tr>

						<tr>
							<td>Нужно ли вам стекло?</td>
							<td>
								<select name="steklo" onchange="checkPaspartu()">
									<option value="Нет">Нет</option>
									<option value="Да">Да</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Нужен ли задник?</td>
							<td>
								<select name="zadnik" onchange="checkPaspartu()">
									<option value="Нет">Нет</option>
									<option value="Да">Да</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><input name="pricex" type="number" value="215.6" hidden disabled></td>
							
						</tr>
					</tbody>
				</table>
				<div class="total">Примерная стоимость: <span class="total">0 руб.</span></div>
				</div>
					<div class="result">Ваша заявка успешно отправлена!</div>
					<table class="form">
						<colgroup>
							<col width="70%">
							<col width="30%">
						</colgroup>
						<tbody>
							<tr>
								<td>Офис</td>
								<td>
									<select name="name">
										<option value="ЧЕРНЫШЕВСКАЯ" selected>«ЧЕРНЫШЕВСКАЯ» пр.Чернышевского,д.17
(во дворе)</option>
										<option value="СЕННАЯ">«СЕННАЯ» Московский проспект,д.5
(во дворе)</option>
										<option value="ВАСИЛЕОСТРОВСКАЯ">«ВАСИЛЕОСТРОВСКАЯ» Средний пр. В.О.,д.36/40
БЦ "Густав"</option>
									</select>
								</td>
								<td></td>
							</tr>
							<td>Ваш внутренний номер заказа</td>
							<td><input name="vn_number" type="text" placeholder="Введите внутренний номер">
							</td>
							<tr>
								<td>Телефон</td>
								<td><input name="phone" type="text" placeholder="Введите телефон"></td>
								<td></td>
							</tr>
							<tr>
								<td>Email</td>
								<td><input name="mail" type="text" placeholder="Введите почту"></td>
								<td></td>
							</tr>
							<td>Комментарий к заказу</td>
							<td><textarea name="comments" rows="7" cols="35" placeholder="Введите примечание к заказу, к примеру, точный номер артикула багета"></textarea>
							</td>
							<tr>
								<td colspan="3">
									<input name="price" type="hidden">
									<input name="form" type="hidden" value="Форма багета">
									<button type="submit">Отправить</button>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>

          </div>
        </div>
      </div>
    </section>
    <?php include 'parts/footer.php'; ?>